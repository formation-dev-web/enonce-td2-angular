TD2 Angular − Gestion de collection de livres
==============================================

Nous allons réaliser une application de gestion de collection de livres, basée
sur l'API que vous avez développée au cours du module JEE.

- **Attention, vous ne partirez pas d'un fork pour ce TD** ;
- **commitez après chaque question**.

Exo 1 : initialisation de l'application
----------------------------------------

<details><summary>AIDE : notions nécessaires</summary>
*outil `ng`, structure de fichiers d'un projet angular, git.*
</details>

- Initialisez une application angular « *td2-angular-livres* » (notez que cela
  initialise un dépôt git)
- sur framagit :
    - créez un dépôt *distant* vierge nommé `td2-angular-livres`
    - donnez les droits « développeur » au formateur
- liez votre dépôt local à ce dépôt distant <details><summary>AIDE</summary>la
  page framagit de *tout dépôt vide indique comment faire ce lien (partie «
  existing git repository »)*</details>
- lancez localement votre app angular, et ouvrez-la dans votre navigateur
- personalisez le squelette de base affiché (au minimum le `<title>` et le `<h1>`)

BONUS :

- Téléchargez et incluez la feuille de style du framework
  CSS [bootstrap](https://getbootstrap.com/) (déposez les fichiers de bootstrap
  dans le dossier *assets/*). Vous utiliserez bootstrap pour donner du style à
  votre page dans la suite du TD.
- Utilisez
  un [jumbotron](https://getbootstrap.com/docs/4.0/components/jumbotron/) pour
  afficher le titre général.

*NB: N'utilisez que la partie CSS de Bootstrap, **sans inclure le JavaScript**
  (cela vous privera de certaines fonctionalités, mais vous évitera des
  conflits avec le code d'Angular).*

Exo 2 : composant détail d'un livre
------------------------------

Nous allons réaliser un composant affichant le détail d'**un** livre.

<details><summary>AIDE : notions nécessaires</summary>
*classes TypeScript, imports TypeScript, templates, data-binding, `ng generate component`*
</details>

- Créez le fichier *src/app/models/book.class.ts* contenant une **classe** «
  `Book` » définissant un livre possédant les champs listés dans la
  spécification de l'API.
- Créez un **composant** « *book-detail* »
- définisssez dans votre classe `BookDetailComponent` une propriété `book` qui
  contient une instance de `Book`.
- Dans le `ngOnInit()` de votre composant, remplissez en dur, (pour l'instant)
  cette propriété `book` avec un livre de votre choix.
- Affichez à l'utilisateur les différentes information relatives au livre via
  la template de votre composant.

BONUS:

- `http://ec1.images-amazon.com/images/P/2930204877.jpg` est l'adresse de la
  couverture du livre dont l'ISBN est 2930204877, cela marche de manière
  similaire pour tous les ISBN. Utilisez cette astuce pour afficher la
  couverture du livre.

- Utilisez une [card](https://getbootstrap.com/docs/4.0/components/card/)
  bootstrap pour mettre en forme les infos à propos du livre.


Exo 3 : composant liste de livres
---------------------------------

Nous allons ajouter un tableau capable de représenter une liste de livres. Pour
le moment, ce composant ne sera pas lié à notre composant `book-detail`.

<details><summary>AIDE : nouvelles notions nécessaires</summary>
*`*ngFor`, `ngClass` pipes de templates*.
</details>

- Créez un nouveau composant `book-list`
- dans le HTML de votre composant, écrivez le code pour lister les livres
  dans une `<table>` à deux colones (titre et premier auteur).
- affichez (sans utiliser CSS) le nom de l'auteur EN MAJUSCULES (trouvez le bon
  *pipe*)
- Dans le CSS, définissez une classe par *catégorie* d'ouvrage, ex:
```css
.categorie-roman {color: red}
.categorie-bd {color: blue}
.categorie-comics {color: green}
```
- Utilisez `ngClass` pour appliquer ces classes CSS selon la propriété
  `category` des ouvrages.

BONUS:

- Faites en sorte que si la liste de livres est vide, la `<table>` n'apparaisse
  pas, mais qu'à la place on aie un `<p>` contenant le message « Aucun livre à
  afficher » (testez en définissant temporairement `this.books = []` dans le
  contrôleur)
- Trouvez le bon *pipe* pour afficher la première lettre de chaque mot du titre de l'ouvrage en majuscule (ex: *« Le Guide Du Voyageur Galactique »*).
- Pour les couleurs, utilisez
  les
  [classes CSS de couleur BootStrap](https://getbootstrap.com/docs/4.0/utilities/colors/) au
  lieu de vos propres classes CSS de couleurs.

Exo 4 : Formulaire de filtrage
------------------------------

<details><summary>AIDE : notions nécessaires</summary>
*`*ngIf`, `ngModel`, méthodes disponibles sur les objets de type String en JavaScript*
</details>

- Offrez un champ de recherche (`<input type="text">`) qui filtre en direct la
  liste de livres par titre (n'affiche que les livres dont le titre contient la
  chaîne saisie).

BONUS:

- Ajoutez un `<select>` qui permette de choisir si le champ de texte filtre la
  liste par titre, auteur, ou bien ISBN. Implémenter son comportement.
- Ajoutez au tableau une colone contenant un bouton de suppression qui permet
  de retirer un élément de la liste des livres (pas simplement de l'affichage
  mais bien de l'attribut `books` de votre contrôleur)

Exo 5 : Formulaire d'ajout
--------------------------

<details><summary>AIDE : notions nécessaires</summary>
*`*ngIf`, `ngModel`, `ngSubmit`.*
</details>

- Ajoutez, dans *book-list.component.html* un `<form>` muni des champs
  suivants :
    - ISBN
    - titre
    - genre
    - nombre de pages
    - auteur (on ne gèrera qu'un auteur unique via ce champ)
    - catégorie
    - éditeur

- Implémentez dans votre *book-list.component.html* et *book-list.component.ts*
  le fait qu'à la soumission du formulaire, notre nouveau livre soit ajouté à
  la fin de la liste `books` du contrôleur.

BONUS

<details><summary>AIDE (doc)</summary>
[Cette doc](https://angular.io/guide/form-validation) peut vous aider beaucoup. Attention, on cherche à faire de la *Template-driven validation* et non de la *Reactive form validation*.
</details>

- Réaliser la validation du formulaire avec Angular : afficher avec une bordure
  rouge les champs invalides, les règles suivantes sont à vérifier :
    - tous les champs sont requis
    - vérification de la longueur de l'ISBN
- Empêcher la soumission du formulaire (attribut `disabled=true` sur le bouton
  de soumission) tant que le formulaire n'est pas valide.

Exo 6 : liaison
---------------
<details><summary>AIDE (notions nécessaires)</summary>
*`@Input()`*
</details>

On veut lier `book-list` à `book-detail` de manière à pouvoir afficher dans
notre composant `book-detail`, le détail de nimporte quel élément de
la liste  (au clic sur un lien dans la liste).

- ajoutez une propriété `detailedBook` sur le contrôleur `book-list`, qui
  contiendra le livre actuellement affiché.
- créez une méthode `afficheDetail(livre : Livre)` sur votre contrôleur de
  liste qui change la valeur de `detailedBook`
- Ajoutez à *chaque ligne* du tableau un lien (`<a href="#">détails</a>`)
- Faites en sorte que ces lien, au clic, appellent la méthode `afficheDetail`
  avec le bon livre.
- Vérifiez que votre composant `book-detail` est
  bien appelé via *book-list.component.html*, et non depuis
  *app.component.html*. Corrigez au besoin.

- déclarer la propriété `book` de `book-detail` en Input.
- passer la propriété `detailedBook` en entrée à votre composant `book-detail`
- enjoy :)


TD3 Angular
===========

Exo 6 : Services (pour de faux)
-------------------------------
<details><summary>AIDE (notions nécessaires)</summary>
*`ng generate service`, injection de dépendance*
</details>


Nous allons créer un service permettant de récupérer une liste de livres, par
la suite, ce service communiquera avec votre API « pour de vrai ».

- Créer un service `books`, et ajoutez-y une méthode permettant de récupérer
  une liste de livres (`Book[]`). Pour l'instant, renvoyez en dur.

- Utilisez votre service depuis votre composant `book-list` pour récupérer la
  liste de livres.


BONUS :

- pas de bonus ici, mais il vous en reste bien un bonus précédent ? :-)

Exo 7 : Services sur API HTTP
------------------------------
<details><summary>AIDE (notions nécessaires)</summary>
*`ng generate service`, injection de dépendance*, `HttpModule`, `Observable`.
</details>

Prérequis :

- Assurez-vous d'avoir une API de gestion de livres fonctionelles et respectant
la [spécification d'API](https://framagit.org/ecn-dw3/jee-book-collection)
fournie dans le module JEE.
- Assurez-vous d'avoir l'extension « CORS Everywhere » installée sur Firefox
  (et activée, le temps du TD uniquement).

C'est parti :

- En utilisant les `Observable` et le module `httpClient`, transformez la
  méthode qui renvoyait une liste de livres:

    - avant: renvoie une liste en dur de type `Book[]`
    - après: interroge l'API et renvoie un `Observable<Book[]>`
- Modifiez le code de votre composant en conséquence

BONUS:

- Faites en sorte également que l'ajout d'un livre via le formulaire sauvegarde
  le livre sur l'API (exo 5).

BONUS

NB: je vous conseille de faire *exo 8 : routing BONUS avant ce bonus*

Nous allons créer un nouveau service permettant de récupérer davantage
d'informations sur le livre via une API externe en récupérant des infos via
l'ISBN.

→ [Doc simplifiée de l'API google books](http://vincent-lecomte.blogspot.fr/2014/04/web-recherche-dun-livre-avec-google.html)


- Testez l'API à la main dans la barre d'URL de votre navigateur avec quelques
  ISBN
- Créez un nouveau service *google-books-api*
- Implémentez-y une méthode `getBookDetails(isbn: string) Observable<{}>` qui
  renvoie les infos de l'API google books pour un ISBN donné.
- Exploitez ce service depuis votre `book-detail.component` pour afficher le
  nombre de pages et l'année de publication
- Pensez bien à gérer le cas où le livre n'est pas trouvé sur l'API (fréquent)
- Écriver une classe `GoogleBooksInfo` contenant deux propriétés : (`pageCount`
  et `publishedDate`).
- Ré-implémentez la méthode de votre service sous la forme
  `getBookDetails(isbn: string) Observable<GoogleBooksInfo>` afin de ne pas
  renvoyer d'information superflue aux composants.


Exo 8 : routing BONUS
---------------------

Mettez en place un routage simple, avec les URL suivantes :

- **`/`** affiche le composant `BookListcomponent`
- **`/about`** affiche une page « à propos » (nécessite de créer un nouveau
  composant pour remplir cette fonction)
